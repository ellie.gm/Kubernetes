from http.server import BaseHTTPRequestHandler, HTTPServer


# HTTPRequestHandler class
class Serve(BaseHTTPRequestHandler):

    # GET method
    def do_GET(self):

        if self.path == '/':
            self.path = '/index.html'
            # Send message back to client
            #message = "Hello world!"
        try:
            file_to_open = open(self.path[1:]).read()
            self.send_response(200)
        except:
            file_to_open = "File not found"
            self.send_response(404)

        self.end_headers()
        # Write content as utf-8 data and display it
        #self.wfile.write(bytes(message, "utf8"))
        self.wfile.write(bytes(file_to_open, 'utf-8'))
        return


def runServer():
    print('server is starting...')
    server_address = ('localhost', 8888)
    httpd = HTTPServer(server_address, Serve)
    print('server is running on 8888...')
    httpd.serve_forever()


runServer()