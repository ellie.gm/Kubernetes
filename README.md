## Kubernetes

Application deployment on Kubernetes guide.


### Installation

Clone the repository:

`$ git clone https://github.com/elnazgm/Kubernetes_Deployment.git`

### GCP Kubernetes deployment steps

1) Install the GCP SDK on your system
2) In order to communicate with Kuberentes install command line tool: `$ gcloud components install kubectl`
3) Create project on GCP
4) Set the project ID on your local bash: `$ gcloud config set project ID`
5) Create cluster on GCP and set the crediantials on your local bash
6) Create the deployment file: `$ kubectl create -f filename.yaml`
7) Verify the pods: `$ kubectl get pods`, `$ kubectl logs -f podname`
8) Verfiy the deployment: `$ kubectl get deployments`, `$ kubectl describe deployment`
9) Create a temporary connection between your localhost and a pod: $ kubectl port-forward podname 3000:3000
10) Apply the changes in Yaml file: `$ kubectl apply -f filename.yaml`
11) Verify the service: `$ kubectl get service`
12) Delete the entire deployment: `$ kubectl delete -f filename.yaml`
13) Delete the container cluster: `$ gcloud container clusters delete`











